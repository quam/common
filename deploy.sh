#!/bin/sh

# deploy ico
cp favicon.ico ../crafting/src/favicon.ico
cp favicon.ico ../homebrew/src/favicon.ico
cp favicon.ico ../player-material/src/favicon.ico
cp favicon.ico ../setting/src/favicon.ico

# deploy styles
cp styles.scss ../crafting/src/styles.scss
cp styles.scss ../homebrew/src/styles.scss
cp styles.scss ../player-material/src/styles.scss
cp styles.scss ../setting/src/styles.scss

# deploy assets
rm -rf ../crafting/src/assets/*
cp assets/* ../crafting/src/assets/

rm -rf ../homebrew/src/assets/*
cp assets/* ../homebrew/src/assets/

rm -rf ../player-material/src/assets/*
cp assets/* ../player-material/src/assets/

rm -rf ../setting/src/assets/*
cp assets/* ../setting/src/assets/

# deploy widgets
rm -rf ../crafting/src/app/common/*
cp -r common/* ../crafting/src/app/common/

rm -rf ../homebrew/src/app/common/*
cp -r common/* ../homebrew/src/app/common/

rm -rf ../player-material/src/app/common/*
cp -r common/* ../player-material/src/app/common/

rm -rf ../setting/src/app/common/*
cp -r common/* ../setting/src/app/common/